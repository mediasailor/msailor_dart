import 'dart:io';
import 'dart:isolate';

import 'calls.dart';

class Log extends Calls {
  void debug(String data) {
    this._printLog("DEBUG   ", data);
  }

  void info(String data) {
    this._printLog("INFO    ", data);
  }

  void warning(String data) {
    this._printLog("WARNING ", data);
  }

  void critical(String data) {
    this._printLog("CRITICAL", data);
  }

  void error(String data) {
    this._printLog("ERROR   ", data);
  }

  void fatal(String data) {
    this._printLog("FATAL   ", data);
  }

  void _printLog(String logLevel, String data) {
    String logTrace = "[" +
        logLevel +
        "] " +
        "[" +
        Isolate.current.debugName.toString() +
        "] " +
        "[" +
        this._getDateTime() +
        "] " +
        data;
    _logAdd(_tracePrint(logTrace, LogType.file));
    print(_tracePrint(logTrace, LogType.console));
  }

  /// Add log trace to log file
  ///
  /// * **param**: trace.
  /// * **return**: void.
  void _logAdd(String trace) {
    DateTime _dateTime = new DateTime.now();
    String logPath = this.data.configuration.logPath();

    Directory historyDirectory = Directory(
        logPath + Platform.pathSeparator + _dateTime.year.toString()); // year
    if (!historyDirectory.existsSync())
      historyDirectory.create(recursive: true);

    historyDirectory = Directory(logPath +
        Platform.pathSeparator +
        _dateTime.year.toString() +
        Platform.pathSeparator +
        _dateTime.year.toString() +
        '-' +
        (_dateTime.month < 10
            ? '0' + _dateTime.month.toString()
            : _dateTime.month.toString())); // month
    if (!historyDirectory.existsSync())
      historyDirectory.create(recursive: true);

    File historyFile = File(historyDirectory.path +
        Platform.pathSeparator +
        _dateTime.toString().split(' ')[0]); // day (file)
    if (!historyFile.existsSync()) historyFile.createSync(recursive: true);

    historyFile.writeAsString('[' + _dateTime.toString() + '] ' + trace + '\n',
        mode: FileMode.append);
  }

  /// Format trace result to be printed in console or file
  ///
  /// * **param**: trace.
  /// * **return**: formatted trace.
  String _tracePrint(String trace, LogType logType) {
    String traceString = trace.toString();
    if (traceString.contains("\\n") || traceString.contains("\\r")) {
      traceString = traceString.replaceAll("\\n", " ");
      traceString = traceString.replaceAll("\\r", " ");
    }

    int length = 0;

    if (logType == LogType.console) length = 300;
    if (logType == LogType.file) length = 600;

    if (traceString.length > length)
      traceString = traceString.substring(0, length) + "...";

    return traceString;
  }

  String _getDateTime() {
    DateTime dateTime = new DateTime.now();
    return dateTime.toString();
  }
}

enum LogType { console, file }
