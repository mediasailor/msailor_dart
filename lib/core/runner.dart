/// Force use cases to override method "run" to create all the logic inside that method.
///
/// * **author**: iruzo
abstract class Runner<T> {
  ///Method that will be executed in the use case. Remember when overrided to establish async.
  Future<T> run();
}
