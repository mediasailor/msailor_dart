import 'package:msailor/core/logic.dart';
import 'package:web_scraper/web_scraper.dart';

/// Send data to IP direction through UDP socket.
///
/// * **author**: iruzo
/// * **params**: URL and path to the desire label (e.g. h3.title > a.caption | or just html, head or body).
/// * **return**: All the web elements as JSON.
/// * **WARNING**: If the web contains javascript to load something dinamically, the returned JSON will probably contain it too and the JSON structure could be incomplete or wrong.
class WebScrapping extends Logic<String> {
  /// Send data to IP direction through UDP socket.
  ///
  /// * **author**: iruzo
  /// * **params**: URL and path to the desire label (e.g. h3.title > a.caption | or just html, head or body).
  /// * **return**: All the web elements as JSON.
  /// * **WARNING**: If the web contains javascript to load something dinamically, the returned JSON will probably contain it too and the JSON structure could be incomplete or wrong.
  WebScrapping(this._url, this._labelPath);

  String _url;
  String _labelPath;

  @override
  Future<String> run() async {
    List<String> urlIndexMatches =
        _getUrlIndexMatches(this._url, new RegExp(r'(.+?)\/\/(.+?)\/'))
            .toList();
    String urlIndex =
        urlIndexMatches.length == 0 ? this._url : urlIndexMatches[0];
    urlIndex = urlIndex.substring(0, urlIndex.length - 1);

    String urlRoute = this._url.substring(urlIndex.length);

    final webScraper = WebScraper(urlIndex);

    List<Map<String, dynamic>> elements = [];
    if (await webScraper.loadWebPage(urlRoute)) {
      elements = webScraper.getElement(this._labelPath, []);
    }

    return elements.toString();
  }

  Iterable<String> _getUrlIndexMatches(String text, RegExp regExp) =>
      regExp.allMatches(text).map((m) => m.group(0)!);
}
