import 'package:msailor/service/library/job/add.dart';
import 'package:msailor/service/library/job/delete.dart';
import 'package:msailor/service/library/job/read.dart';

/// Library manager
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Library {
  /// Library manager
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Library();

  /// Read entry in library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * [OPTIONAL] entry (just String inside de value saved in a list)
  ///   * [OPTIONAL] listName
  ///   * [OPTIONAL] file (if checked, it will return the file with entry as name, and if there is no entry, it return all files)
  ///     * **How this works**
  ///     * If you set entry and list, the entry will be returned from that list.
  ///     * If you only set entry, the entry will be returned from every list.
  ///     * If you only set list, the entire list will be returned.
  ///     * If entry and list are null, all lists will be returned.
  /// * **return**: Map<String, dynamic> where 'dynamic' .
  /// * **WARNING**: The files will be returned as Map<String, dynamic> inside the value, not the key.
  Future<Map<String, dynamic>> read(String libraryFolderPath,
          {String? entry, String? listName, bool? file}) =>
      Read(libraryFolderPath, entry: entry, listName: listName, file: file)
          .call();

  /// Add entry to library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * entry (json as string)
  ///   * listName (optional - name of the list you want to be added).
  ///   * [OPTIONAL] pathToCopyFrom (path to file you want to add to the library).
  /// * **return**: void.
  add(String libraryFolderPath, String entry, String listName,
          {String? pathToCopyFrom}) =>
      Add(libraryFolderPath, entry, listName, pathToCopyFrom: pathToCopyFrom)
          .call();

  /// Delete entry in library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * [OPTIONAL] entry (just String inside de value saved in a list)
  ///   * [OPTIONAL] listName
  ///   * [OPTIONAL] removeFile (if checked, it will delete the file with entry as name, and if there is no entry, it will delete all files)
  ///     * **How this works**
  ///     * If you set entry and list, the entry will be deleted from that list.
  ///     * If you only set entry, the entry will be deleted from every list.
  ///     * If you only set list, the entire list will be deleted.
  ///     * If entry and list are null, all lists will be deleted.
  /// * **return**: void.
  delete(String libraryFolderPath,
          {String? entry, String? listName, bool? removeFile}) =>
      Delete(libraryFolderPath,
              entry: entry, listName: listName, removeFile: removeFile)
          .call();
}
