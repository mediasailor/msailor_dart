import 'package:msailor/service/msailor/job/youtube_channel_name.dart';
import 'package:msailor/service/msailor/job/youtube_download.dart';
import 'package:msailor/service/msailor/job/youtube_search.dart';
import 'package:msailor/model/media_info.dart';

/// Msailor functionalities
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Msailor {
  /// Msailor functionalities
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Msailor();

  /// Search for videos in youtube.
  ///
  /// * **author**: iruzo
  /// * **params**: Query to send to youtube.
  /// * **return**: Map with videos information.
  Future<List<MediaInfo>> youtubeSearch(String query) =>
      YoutubeSearch(query).call();

  /// Get channel name from channel url.
  ///
  /// * **author**: iruzo
  /// * **params**: Youtube channel url.
  /// * **return**: Channel name as String.
  Future<String> youtubeChannelName(String channelUrl) =>
      YoutubeChannelName(channelUrl).call();

  /// Download youtube video.
  ///
  /// * **author**: iruzo
  /// * **params**: MediaInfo with youtube video info.
  /// * **return**: void.
  Future<void> youtubeDownload(MediaInfo mediaInfo) =>
      YoutubeDownload(mediaInfo).call();
}
