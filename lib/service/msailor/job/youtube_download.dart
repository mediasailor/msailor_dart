import 'dart:io';

import 'package:msailor/core/logic.dart';
import 'package:msailor/model/media_info.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';

/// Download youtube video.
///
/// * **author**: iruzo
/// * **params**: MediaInfo with youtube video info.
/// * **return**: void.
class YoutubeDownload extends Logic<void> {
  /// Download youtube video.
  ///
  /// * **author**: iruzo
  /// * **params**: MediaInfo with youtube video info.
  /// * **return**: instance.
  YoutubeDownload(this._mediaInfo);

  MediaInfo _mediaInfo;

  @override
  Future<void> run() async {
    YoutubeExplode yt = YoutubeExplode();
    StreamManifest manifest =
        await yt.videos.streamsClient.getManifest(this._mediaInfo.id);
    AudioOnlyStreamInfo streamInfo = manifest.audioOnly.withHighestBitrate();
    var stream = yt.videos.streamsClient.get(streamInfo);

    var file = File(this.data.configuration.libraryPath() +
        Platform.pathSeparator +
        'file' +
        Platform.pathSeparator +
        this._mediaInfo.title!);
    if (!file.existsSync()) file.createSync(recursive: true);
    var fileStream = file.openWrite();

    this.service.history.add(this.data.configuration.downloadHistoryPath(),
        '[downloading]-' + this._mediaInfo.title!);

    await stream.pipe(fileStream);

    this.service.history.delete(this.data.configuration.downloadHistoryPath(),
        entry: '[downloading]-' + this._mediaInfo.title!);
    this.service.history.add(
        this.data.configuration.downloadHistoryPath(), this._mediaInfo.title!);

    await fileStream.flush();
    await fileStream.close();

    yt.close();
  }
}
