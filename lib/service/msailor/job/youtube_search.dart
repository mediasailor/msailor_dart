import 'dart:convert';

import 'package:msailor/core/logic.dart';
import 'package:msailor/model/media_info.dart';

/// Search for videos in youtube.
///
/// * **author**: iruzo
/// * **params**: Query to send to youtube.
/// * **return**: Map with videos information.
class YoutubeSearch extends Logic<List<MediaInfo>> {
  /// Search for videos in youtube.
  ///
  /// * **author**: iruzo
  /// * **params**: Query to send to youtube.
  /// * **return**: Map with videos information.
  YoutubeSearch(this._query);

  String _query;

  @override
  Future<List<MediaInfo>> run() async {
    if (this._query == '' || this._query == ' ')
      return [];

    this
        .service
        .history
        .add(this.data.configuration.searchHistoryPath(), this._query);

    String youtubeQuery = 'https://www.youtube.com/results?search_query=' +
        this._query.trim().replaceAll(' ', '+');
    String scrappedPage =
        await this.service.net.webScrapping(youtubeQuery, 'body');

    var results = _decodedScrappedPageResults(scrappedPage);

    List<MediaInfo> videos = [];
    for (var result in results) {
      bool live = false;
      bool radio = false;
      if (result.toString().contains("videoRenderer") &&
          !result.toString().contains("shelfRenderer")) {
        if (!result['videoRenderer']['viewCountText']
            .toString()
            .contains('simpleText')) live = true;
        if (!live && result['videoRenderer']['lengthText'] == null)
          radio = true;

        String channelSubDirUrl = result['videoRenderer']['ownerText']['runs']
                [0]['navigationEndpoint']['commandMetadata']
            ['webCommandMetadata']['url'];
        String channelName = channelSubDirUrl;
        if (channelSubDirUrl.contains('/user/')) {
          channelName = channelName.split('/user/')[1];
        }

        videos.add(MediaInfo(
            source: 'youtube',
            id: result['videoRenderer']['navigationEndpoint']['watchEndpoint']
                ['videoId'],
            url: result['videoRenderer']['navigationEndpoint']
                ['commandMetadata']['webCommandMetadata']['url'],
            live: live,
            channelName: channelName,
            channelSubDirUrl: channelSubDirUrl,
            channelAvatarUrl: result['videoRenderer']
                        ['channelThumbnailSupportedRenderers']
                    ['channelThumbnailWithLinkRenderer']['thumbnail']
                ['thumbnails'][0]['url'],
            title: result['videoRenderer']['title']['runs'][0]['text'],
            videoPicUrl: result['videoRenderer']['thumbnail']['thumbnails'][0]
                ['url'],
            length:
                live || radio ? '' : result['videoRenderer']['lengthText']['simpleText'],
            views: live ? result['videoRenderer']['viewCountText']['runs'][0]['text'] : result['videoRenderer']['viewCountText']['simpleText'].split(" ")[0]));
      }
    }

    return videos;
  }

  dynamic _decodedScrappedPageResults(String scrappedPage) {
    // clean the javascript from the JSON
    scrappedPage = scrappedPage.substring(
        scrappedPage.toString().indexOf('{"responseContext":'),
        scrappedPage
            .toString()
            .indexOf(";if (window.ytcsi) {window.ytcsi.tick('pdr',"));

    return json.decode(scrappedPage)['contents']
                ['twoColumnSearchResultsRenderer']['primaryContents']
            ['sectionListRenderer']['contents'][0]['itemSectionRenderer']
        ['contents'];
  }
}
