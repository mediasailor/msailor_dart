import 'dart:io';

import 'package:msailor/core/logic.dart';

/// Add entry to history.
///
/// * **author**: iruzo
/// * **params**: historyFolderPath (e.g. /history), entry.
/// * **return**: void.
class Add extends Logic<void> {
  /// Add entry to history.
  ///
  /// * **author**: iruzo
  /// * **params**: historyFolderPath (e.g. /history), entry.
  /// * **return**: instance.
  Add(this._historyFolderPath, this._entry);

  String _entry;
  String _historyFolderPath;

  @override
  Future<void> run() async {
    if (this._entry.isEmpty || this._entry == '') return null;

    DateTime dateTime = new DateTime.now();

    Directory historyDirectory = Directory(this._historyFolderPath +
        Platform.pathSeparator +
        dateTime.year.toString()); // year
    if (!historyDirectory.existsSync())
      historyDirectory.create(recursive: true);

    historyDirectory = Directory(this._historyFolderPath +
        Platform.pathSeparator +
        dateTime.year.toString() +
        Platform.pathSeparator +
        dateTime.year.toString() +
        '-' +
        (dateTime.month < 10
            ? '0' + dateTime.month.toString()
            : dateTime.month.toString())); // month
    if (!historyDirectory.existsSync())
      historyDirectory.create(recursive: true);

    File historyFile = File(historyDirectory.path +
        Platform.pathSeparator +
        dateTime.toString().split(' ')[0]); // day (file)

    historyFile.writeAsString(
        (!historyFile.existsSync() ? '' : '\n') +
            '[' +
            dateTime.toString() +
            '] ' +
            this._entry,
        mode: FileMode.writeOnlyAppend);
  }
}
