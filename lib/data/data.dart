import 'package:msailor/data/configuration/configuration.dart';
import 'package:msailor/data/memory/memory.dart';

/// Manage all storagement types in the application.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Data {
  /// Manage all storagement types in the application.
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Data();

  /// Aplication configuration properties.
  Configuration configuration = Configuration();

  /// Manage memory allocated.
  Memory memory = Memory();
}
