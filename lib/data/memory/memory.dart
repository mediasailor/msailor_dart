import 'package:msailor/model/media_info.dart';

/// Manage memory allocated.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Memory {
  /// Media queue.
  List<MediaInfo> mediaQueue = _Memory._mediaQueue;
}

class _Memory {
  /// Media queue.
  static List<MediaInfo> _mediaQueue = [];
}
