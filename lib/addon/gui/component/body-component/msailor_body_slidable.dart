import 'package:flutter/material.dart';

import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:msailor/data/data.dart';
import 'package:msailor/service/service.dart';

import 'package:msailor/model/media_info.dart';

class MsailorBodySlidable extends StatefulWidget {
  final MediaInfo mediaInfo;

  MsailorBodySlidable(this.mediaInfo);

  @override
  State<StatefulWidget> createState() => _MsailorBodySlidableState(this.mediaInfo);
}

class _MsailorBodySlidableState extends State<MsailorBodySlidable> {
  MediaInfo _mediaInfo;

  _MsailorBodySlidableState(this._mediaInfo);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
        color: Colors.grey[900],
        child: ListTile(
          leading: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.network(this._mediaInfo.videoPicUrl!)),
          title: Row(
            children: [
              Text(this._mediaInfo.title!),
              Expanded(
                  child: Text(
                '',
                textAlign: TextAlign.right,
              )),
              Text(
                this._mediaInfo.length! + ' ',
                style: TextStyle(
                  color: Colors.cyan,
                ),
              ),
              Visibility(
                  visible: this._mediaInfo.live!,
                  child: Image.asset(
                    'asset/gif/live.gif',
                    scale: 7,
                  )),
              Visibility(
                visible: this._mediaInfo.source != null &&
                    this._mediaInfo.source!.isNotEmpty &&
                    !this._mediaInfo.source!.toLowerCase().contains('history'),
                child: Image.asset(
                    'asset/img/' + this._mediaInfo.source! + '-logo.webp',
                    alignment: Alignment.centerRight,
                    scale: 40),
              )
            ],
          ),
          subtitle: Row(
            children: [
              Visibility(
                visible: this._mediaInfo.channelAvatarUrl != null &&
                    this._mediaInfo.channelAvatarUrl!.isNotEmpty,
                child: CircleAvatar(
                  radius: 10,
                  backgroundImage:
                      NetworkImage(this._mediaInfo.channelAvatarUrl!),
                ),
              ),
              Text(' ' + this._mediaInfo.channelName!),
              Visibility(
                visible: this._mediaInfo.views != null &&
                    this._mediaInfo.views!.isNotEmpty,
                child: Expanded(
                  child: Text(
                    this._mediaInfo.views! +
                        (this._mediaInfo.live! ? ' viewers' : ' views'),
                    textAlign: TextAlign.right,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      key: Key((this._mediaInfo.id == null || this._mediaInfo.id!.isEmpty)
          ? this._mediaInfo.title!
          : this._mediaInfo.id!),
      dismissal: SlidableDismissal(
        key: Key((this._mediaInfo.id == null || this._mediaInfo.id!.isEmpty)
            ? this._mediaInfo.title!
            : this._mediaInfo.id!),
        dragDismissible: true,
        child: SlidableDrawerDismissal(),
        onDismissed: (direction) {
            if(this._mediaInfo.source!.contains('History')) {
                Service().history.delete(
                        identical(this._mediaInfo.source!, 'searchHistory')
                        ? Data().configuration.searchHistoryPath()
                        : Data().configuration.downloadHistoryPath(),
                        date: this._mediaInfo.title!.split(' ')[0].replaceFirst('[', '') +
                        ' ' +
                        this._mediaInfo.title!.split(' ')[1].replaceFirst(']', ''),
                        entry: this._mediaInfo.title!.split(' ')[2]);
            } else {
                Data().memory.mediaQueue.add(this._mediaInfo);
            }
        },
      ),
      actions: (this._mediaInfo.url == null || this._mediaInfo.url!.isEmpty)
          ? <Widget>[
              IconSlideAction(
                caption: 'Drag to remove',
                color: Colors.red,
                icon: Icons.highlight_remove,
              ),
            ]
          : <Widget>[
              IconSlideAction(
                caption: 'Add to queue',
                color: Colors.grey[900],
                icon: Icons.playlist_play,
                onTap: () => Data().memory.mediaQueue.add(this._mediaInfo),
              ),
            ],
      secondaryActions:
          (this._mediaInfo.url == null || this._mediaInfo.url!.isEmpty)
              ? []
              : <Widget>[
                  IconSlideAction(
                    caption: 'Favorite',
                    color: Colors.grey[900],
                    icon: Icons.star,
                    onTap: () => _showSnackBar('favorite'),
                  ),
                  IconSlideAction(
                    caption: 'Add to list',
                    color: Colors.grey[900],
                    icon: Icons.playlist_add,
                    onTap: () => _showSnackBar('addToList'),
                  ),
                  Visibility(
                      visible: !this._mediaInfo.live!,
                      child: IconSlideAction(
                        caption: 'Download',
                        color: Colors.grey[900],
                        icon: Icons.download_sharp,
                        onTap: () {
                          if (!this._mediaInfo.live!)
                            Service().msailor.youtubeDownload(this._mediaInfo);
                        },
                      ))
                ],
    );
  }

  _showSnackBar(String temp) {}
}
