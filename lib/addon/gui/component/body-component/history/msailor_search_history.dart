import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/body-component/history/msailor_history.dart';

class MsailorSearchHistory extends StatelessWidget {
  MsailorSearchHistory(this._historyPath);

  final String _historyPath;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MsailorHistory('searchHistory', this._historyPath),
    );
  }
}
