import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/body-component/history/msailor_history.dart';

class MsailorDownloadHistory extends StatelessWidget {
  MsailorDownloadHistory(this._historyPath);

  final String _historyPath;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MsailorHistory('downloadHistory', this._historyPath),
    );
  }
}
