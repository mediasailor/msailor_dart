import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/end-drawer-component/msailor_end_drawer_slidable.dart';
import 'package:msailor/data/data.dart';

class MsailorEndDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MsailorEndDrawerState();
}

class _MsailorEndDrawerState extends State<MsailorEndDrawer> {
  List<MsailorEndDrawerSlidable> _children = Data()
          .memory
          .mediaQueue
          .map((e) => MsailorEndDrawerSlidable(e))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: this._children,
      ),
    );
  }
}
