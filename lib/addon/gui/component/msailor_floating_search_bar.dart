import 'package:flutter/material.dart';
import 'dart:async';

import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:msailor/addon/gui/component/body-component/msailor_body_slidable.dart';
import 'package:msailor/service/service.dart';
import 'package:msailor/model/media_info.dart';

class MsailorFloatingSearchBar extends StatefulWidget {
  final StreamController<Widget> streamController;

  MsailorFloatingSearchBar(this.streamController);

  @override
  State<StatefulWidget> createState() => _MsailorFloatingSearchBarState();
}

class _MsailorFloatingSearchBarState extends State<MsailorFloatingSearchBar> {
  List<MediaInfo> result = [];
  final GlobalKey<_MsailorBodyState> _msailorBodyState = GlobalKey<_MsailorBodyState>();

  void _updateBody(Widget newBody) {
    setState(() {
      this._msailorBodyState.currentState!.updateBody(newBody);
    });
  }

  @override
  void initState() {
    super.initState();
    widget.streamController.stream.listen((newBody) {
      _updateBody(newBody);
    });
  }

  @override
  Widget build(BuildContext context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return FloatingSearchBar(
      hint: 'Search...',
      scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
      transitionDuration: const Duration(milliseconds: 800),
      transitionCurve: Curves.easeInOut,
      physics: const BouncingScrollPhysics(),
      axisAlignment: isPortrait ? 0.0 : -1.0,
      debounceDelay: const Duration(milliseconds: 500),
      onQueryChanged: (query) {
        Service()
            .msailor
            .youtubeSearch(query)
            .then((value) => setState(() => result = value));
      },
      //onFocusChanged: (focus) => {
      //if (focus)
      //{
      //Service()
      //.history
      //.read(Data().configuration.searchHistoryPath(),
      //entry: new DateTime.now().toString().split(' ')[0] +
      //' 00:00:00.000000')
      //.then((value) => setState(() => {
      //result = value
      //.map((e) => MediaInfo(
      //source: 'historySearch',
      //id: '',
      //url: '',
      //live: false,
      //channelName: '',
      //channelSubDirUrl: '',
      //channelAvatarUrl: '',
      //title: e,
      //videoPicUrl:
      //'https://baltimoreheritage.org/wp-content/uploads/2014/04/icon_5848-1024x1024.png',
      //length: '',
      //views: ''))
      //.toList()
      //}))
      //}
      //},
      automaticallyImplyDrawerHamburger: false,
      clearQueryOnClose: true,
      transition: CircularFloatingSearchBarTransition(),
      actions: [
        FloatingSearchBarAction(
          showIfOpened: false,
          child: CircularButton(
            icon: const Icon(Icons.list),
            onPressed: () {
              Scaffold.of(context).openEndDrawer();
            },
          ),
        ),
        FloatingSearchBarAction.searchToClear(
          showIfClosed: false,
        ),
      ],
      body: _MsailorBody(_msailorBodyState),
      builder: (context, transition) {
        return this.result.isEmpty
            ? Column(
                children: [],
              )
            : Column(
                children: this.result.map((e) => MsailorBodySlidable(e)).toList());
      },
    );
  }
}

class _MsailorBody extends StatefulWidget {
  _MsailorBody(Key key) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MsailorBodyState();
}

class _MsailorBodyState extends State<_MsailorBody> {
  Widget? _body;

  void updateBody(Widget newBody) {
    setState(() {
      this._body = newBody;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: this._body,
      margin: EdgeInsets.only(top: 60, right: 10, left: 10),
    );
  }
}
