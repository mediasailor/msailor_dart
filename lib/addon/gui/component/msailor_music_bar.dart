import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:msailor/data/data.dart';

class MsailorMusicBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MsailorMusicBarState();
}

class _MsailorMusicBarState extends State<MsailorMusicBar> {
  bool _isPlaying = false;
  final player = AudioPlayer();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("00:00",
              style: TextStyle(
                fontSize: 13,
              )),
          Spacer(),
          TextButton(
            child: Icon(Icons.loop),
            onPressed: () {},
          ),
          TextButton(
            child: Icon(Icons.skip_previous),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(this._isPlaying ? Icons.pause : Icons.play_arrow),
            color: Colors.blue,
            onPressed: () {
                if(Data().memory.mediaQueue.isNotEmpty) {
                    String? mediaPath = Data().memory.mediaQueue.first.url;
                    if(mediaPath != null) {
                        if(mediaPath.contains('https')) {
                            player.setUrl(mediaPath);
                        } else {
                            player.setFilePath(mediaPath);
                        }
                    }
                    this._isPlaying ? player.pause() : player.play();
                    setState(() {
                        this._isPlaying = !this._isPlaying;
                    });
                }
            },
          ),
          TextButton(
            child: Icon(Icons.skip_next),
            onPressed: () {},
          ),
          TextButton(
            child: Icon(Icons.shuffle),
            onPressed: () {},
          ),
          Spacer(),
          Text("00:00",
              style: TextStyle(
                fontSize: 13,
              )),
        ],
      ),
    );
  }
}
