import 'dart:async';

import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/msailor_drawer.dart';
import 'package:msailor/addon/gui/component/msailor_end_drawer.dart';
import 'package:msailor/addon/gui/component/msailor_floating_search_bar.dart';
import 'package:msailor/addon/gui/component/msailor_music_bar.dart';

class Gui extends StatefulWidget {
  @override
  _GuiState createState() => _GuiState();
}

class _GuiState extends State<Gui> {
  StreamController<Widget> _bodyStreamController = StreamController<Widget>();

  @override
  void dispose() {
    this._bodyStreamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: Scaffold(
        drawer: MsailorDrawer(_bodyStreamController),
        body: MsailorFloatingSearchBar(_bodyStreamController),
        endDrawer: MsailorEndDrawer(),
        bottomNavigationBar: MsailorMusicBar(),
      ),
    );
  }
}
